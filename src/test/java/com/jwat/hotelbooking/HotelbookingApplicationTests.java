package com.jwat.hotelbooking;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.jwat.hotelbooking.Models.Category;
import com.jwat.hotelbooking.Models.Guest;
import com.jwat.hotelbooking.Models.RoomReserved;
import com.jwat.hotelbooking.Repositories.CategoryRepository;
import com.jwat.hotelbooking.Services.CategoryServices;
import com.jwat.hotelbooking.Services.GuestServices;
import com.jwat.hotelbooking.Services.NotFoundException;
import com.jwat.hotelbooking.Services.RoomReservedServices;

@SpringBootTest
class HotelbookingApplicationTests {
	@Autowired
	private RoomReservedServices roomReserved;
	@Autowired
	private CategoryServices categoryServices;
	@Autowired
	private GuestServices guestServices;


	@Test
	public void getAllCate(){
		Iterable<Category> cs = categoryServices.getAllCategories();
		Assertions.assertThat(cs).hasSizeGreaterThan(0);

		for(Category c : cs){
			System.out.println(c.getId());
		}
	}

	@Test
	public void getAllGuest(){
		Iterable<Guest> gs = guestServices.getAllGuests();
		Assertions.assertThat(gs).hasSizeGreaterThan(0);

		for(Guest g : gs){
			System.out.println(g.getId());
		}
	}

	//add guest
	@Test
	public void addGuest(){
		Guest g = new Guest();
		g.setGuestFirstName("John");
		g.setGuestLastName("acs");
		g.setGuestGender(true);
		g.setGuestEmail("123123");
		g.setGuestPhone("123123");
		g.setGuestAddress("123123");
		g.setGuestNote("123123");
		guestServices.addGuest(g);

	}

	//delete guest by id
	@Test
	public void deleteGuestById() throws NotFoundException{
		guestServices.delete((long) 2);
	}
}
