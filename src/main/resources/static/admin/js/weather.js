function drawData(d){
    var celcius = Math.round(parseFloat(d.main.temp));
    document.getElementById('tempout').innerHTML = celcius;
}

function weatherBalloon( cityName ) {
    var key = 'd1f3fa0c0fa8abcafd8f079fe5b280e7';
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + cityName+ '&appid=' + key + '&units=metric')  
    .then(function(resp) { return resp.json() }) // Convert data to json
    .then(function(data) {
      drawData(data);
    })
    .catch(function() {
      // catch any errors
    });
  }


  
  window.onload = function() {
    weatherBalloon( 'Ho Chi Minh City' );
  }