package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Room;

public interface RoomRepository extends CrudRepository<Room, Long> {

    public Long countById(Long id);


    
}
    
