package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Invoice;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

    public Long countById(Long id);

}
    
