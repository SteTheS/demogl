package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    public Long countById(Long id);

}

