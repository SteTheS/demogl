package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.jwat.hotelbooking.Models.Type;

public interface TypeRepository extends CrudRepository<Type, Long> {

    public Long countById(Long id);

}
