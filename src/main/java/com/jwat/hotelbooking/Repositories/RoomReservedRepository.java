package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.RoomReserved;

public interface RoomReservedRepository extends CrudRepository<RoomReserved, Long> {

    public Long countById(Long id);

}