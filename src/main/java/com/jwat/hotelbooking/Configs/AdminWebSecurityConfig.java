package com.jwat.hotelbooking.Configs;

import com.jwat.hotelbooking.Services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Order(2)
public class AdminWebSecurityConfig  {
    // @Bean
    // protected void configure(AuthenticationManager  Builder auth) throws Exception {
    // 	PasswordEncoder encoder = 
    //       PasswordEncoderFactories.createDelegatingPasswordEncoder();
    // 	auth
    //       .inMemoryAuthentication()
    //       .withUser("admin")
    //       .password(encoder.encode("admin"))
    //       .roles("ADMIN", "USER");
    // }
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        PasswordEncoder encoder = 
          PasswordEncoderFactories.createDelegatingPasswordEncoder();
        UserDetails user = User.withUsername("admin")
            .password(encoder.encode("admin"))
            .roles("ADMIN", "USER")
            .build();
        return new InMemoryUserDetailsManager(user);
    }


	// @Autowired
	// public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	// 	PasswordEncoder encoder = 
    //          PasswordEncoderFactories.createDelegatingPasswordEncoder();
	// 	auth.inMemoryAuthentication().withUser("admin")
    //         .password(encoder.encode("admin")).roles("ADMIN");

	//      auth.inMemoryAuthentication().withUser("user").password(encoder.encode("admin")).roles("USER");
	// }
    
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.antMatcher("/admin*")
          .authorizeRequests()
          .antMatchers("/admin/**").hasRole("ADMIN")
          .anyRequest()
          .hasRole("ADMIN")
          
          
          .and()
          .formLogin()
          .loginPage("/admin/login")
          .loginProcessingUrl("/admin_login")
          .failureUrl("/admin/login?error")
          .defaultSuccessUrl("/admin")
          
          .and()
          .logout()
          .logoutUrl("/admin_logout")
          .logoutSuccessUrl("/admin")
          .deleteCookies("JSESSIONID")
          
          .and()
          .exceptionHandling()
          .accessDeniedPage("/403")
          
          .and()
          .csrf().disable();
        ;

        return http.build();
    }
    
}