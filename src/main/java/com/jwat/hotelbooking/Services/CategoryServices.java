package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Category;
import com.jwat.hotelbooking.Repositories.CategoryRepository;

@Service
public class CategoryServices {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getAllCategories() {
        return (List<Category>) categoryRepository.findAll();
    }

    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    public Category getCategoryById(Long id) throws NotFoundException{
        Optional<Category> result = categoryRepository.findById(id);
        if(result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Category not found");
        }
    }

    public void delete(Long id) throws NotFoundException{
        Long count = categoryRepository.countById(id);
        if(count == 0) {
            throw new NotFoundException("Category not found");
        } else {
            categoryRepository.deleteById(id);
        }
    }


}
