package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Type;
import com.jwat.hotelbooking.Repositories.TypeRepository;

@Service
public class TypeServices {
    @Autowired
    private TypeRepository typeRepository;

    // list all
    public List<Type> getAllTypes() {
        return (List<Type>) typeRepository.findAll();
    }

    // find by id
    public Type getTypeById(Long id) throws NotFoundException {
        Optional<Type> result = typeRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Type not found");
        }
    }

    // save
    public Type save(Type type) {
        return typeRepository.save(type);
    }

    // delete
    public void delete(Long id) throws NotFoundException {
        Long count = typeRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("Type not found");
        } else {
            typeRepository.deleteById(id);
        }
    }
}
