package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Guest;
import com.jwat.hotelbooking.Repositories.GuestRepository;

@Service
public class GuestServices {
    @Autowired
    private GuestRepository guestRepository;
    
    //list all
    public List<Guest> getAllGuests() {
        return (List<Guest>) guestRepository.findAll();
    }

    //find by id
    public Guest getGuestById(Long id) throws NotFoundException{
        Optional<Guest> result = guestRepository.findById(id);
        if(result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Guest not found");
        }
    }

    //save
    public Guest save(Guest guest) {
        return guestRepository.save(guest);
    }

    //delete
    public void delete(Long id) throws NotFoundException{
        Long count = guestRepository.countById(id);
        if(count == 0) {
            throw new NotFoundException("Guest not found");
        } else {
            guestRepository.deleteById(id);
        }
    }

    public Long countTotalGuests() {
        Long count = guestRepository.count();
        return count;
    }

    public void addGuest(Guest g) {
        guestRepository.save(g);

    }

    public void deleteGuestByName(String string) {
        guestRepository.deleteByGuestFirstName(string);
    }

    
}
