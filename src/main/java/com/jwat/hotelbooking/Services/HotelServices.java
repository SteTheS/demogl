package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Hotel;
import com.jwat.hotelbooking.Repositories.HotelRepository;

@Service
public class HotelServices {
    @Autowired
    private HotelRepository hotelRepository;

    // list all
    public List<Hotel> getAllHotels() {
        return (List<Hotel>) hotelRepository.findAll();
    }

    // find by id
    public Hotel getHotelById(Long id) throws NotFoundException {
        Optional<Hotel> result = hotelRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Hotel not found");
        }
    }

    // save
    public Hotel save(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    // delete
    public void delete(Long id) throws NotFoundException {
        Long count = hotelRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("Hotel not found");
        } else {
            hotelRepository.deleteById(id);
        }
    }
}
