package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Room;
import com.jwat.hotelbooking.Repositories.RoomRepository;

@Service
public class RoomServices {
    @Autowired
    private RoomRepository roomRepository;

    // list all
    public List<Room> getAllRooms() {
        return (List<Room>) roomRepository.findAll();
    }

    // find by id
    public Room getRoomById(Long id) throws NotFoundException {
        Optional<Room> result = roomRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Room not found");
        }
    }

    // save
    public Room save(Room room) {
        return roomRepository.save(room);
    }

    // delete
    public void delete(Long id) throws NotFoundException {
        Long count = roomRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("Room not found");
        } else {
            roomRepository.deleteById(id);
        }
    }

    public Long countTotalRooms() {
        return roomRepository.count();
    }

    public Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

}
