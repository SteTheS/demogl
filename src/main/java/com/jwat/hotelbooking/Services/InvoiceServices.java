package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Invoice;
import com.jwat.hotelbooking.Repositories.InvoiceRepository;

@Service
public class InvoiceServices {
    @Autowired
    private InvoiceRepository invoiceRepository;

    //list all
    public List<Invoice> getAllInvoices() {
        return (List<Invoice>) invoiceRepository.findAll();
    }

    //find by id
    public Invoice getInvoiceById(Long id) throws NotFoundException {
        Optional<Invoice> result = invoiceRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Invoice not found");
        }
    }

    //save
    public Invoice save(Invoice invoice) {
        return invoiceRepository.save(invoice);
    }

    //delete
    public void delete(Long id) throws NotFoundException {
        Long count = invoiceRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("Invoice not found");
        } else {
            invoiceRepository.deleteById(id);
        }
    }
}
