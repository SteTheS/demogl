package com.jwat.hotelbooking.Services;

public class NotFoundException extends Throwable{
    public NotFoundException (String message) {
        super(message);
    }
}
