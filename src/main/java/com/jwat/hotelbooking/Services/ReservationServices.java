package com.jwat.hotelbooking.Services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.Reservation;
import com.jwat.hotelbooking.Repositories.ReservationRepository;

@Service
public class ReservationServices {
    @Autowired
    private ReservationRepository reservationRepository;

    //list all
    public List<Reservation> getAllReservations() {
        return (List<Reservation>) reservationRepository.findAll();
    }

    //find by id
    public Reservation getReservationById(Long id) throws NotFoundException {
        Optional<Reservation> result = reservationRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("Reservation not found");
        }
    }

    //save
    public Reservation save(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    //delete
    public void delete(Long id) throws NotFoundException {
        Long count = reservationRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("Reservation not found");
        } else {
            reservationRepository.deleteById(id);
        }
    }

    //count total reservations
    public Long countTotalReservations() {
        return reservationRepository.count();
    }

    //count reservations by reservation date
    public Long countReservationsByReservationDate(Date reservationDate) {
        return reservationRepository.countByTsReserved(reservationDate);
    }
    
}
