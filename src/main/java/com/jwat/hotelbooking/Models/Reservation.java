package com.jwat.hotelbooking.Models;

import java.util.Date;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "reservation")
@Getter
@Setter
@ConstructorBinding
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Basic(optional = false)
    @Column(name = "tsReserved", nullable = false, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsReserved;

    @Basic(optional = false)
    @Column(name = "tsResEnd", nullable = false, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsResEnd;

    @Basic
    @Column(name = "tsCreated", nullable = false, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsCreated;

    @Basic
    @Column(name = "tsUpdated", nullable = true, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsUpdated;

    @Column(name = "discountPercent", nullable = true, length = 50)
    private Double discountPercent;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "guestId")
    private Guest guest;

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private java.util.List<Invoice> invoices;

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private java.util.List<RoomReserved> roomReserved;
    
}
