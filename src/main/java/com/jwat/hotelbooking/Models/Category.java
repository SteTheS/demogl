package com.jwat.hotelbooking.Models;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "category")
@Getter
@Setter
@ConstructorBinding
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="cateName", nullable = false, length = 50)
    private String cateName;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private java.util.List<Hotel> hotels;
}
