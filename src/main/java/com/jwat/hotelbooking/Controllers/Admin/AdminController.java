package com.jwat.hotelbooking.Controllers.Admin;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jwat.hotelbooking.Models.Category;
import com.jwat.hotelbooking.Models.Guest;
import com.jwat.hotelbooking.Models.Reservation;
import com.jwat.hotelbooking.Models.Room;
import com.jwat.hotelbooking.Models.RoomReserved;
import com.jwat.hotelbooking.Services.*;

@Controller
public class AdminController {
    @Autowired
    private RoomReservedServices roomReserved;
    @Autowired
    private RoomServices roomServices;
    @Autowired
    private GuestServices guestServices;
    @Autowired
    private ReservationServices reservationServices;
    @Autowired
    private TypeServices typeServices;
    @Autowired
    private HotelServices hotelServices;


    @GetMapping("/admin")
    public String dashBoard(Model model) {

        List<Reservation> reservations = reservationServices.getAllReservations();
        Long totalReservations = reservationServices.countTotalReservations();

        Date today=new Date();
        Long todayRes = reservationServices.countReservationsByReservationDate(today);


        model.addAttribute("reservations", reservations);
        model.addAttribute("totalReservations", totalReservations);
        model.addAttribute("todayRes", todayRes);

        

        return "admin/index";
    }

    @GetMapping("/admin/guestlist")
    public String guestList(Model model) {

        List<Guest> guests = guestServices.getAllGuests();
        Long totalGuests = guestServices.countTotalGuests();

        model.addAttribute("guests", guests);
        model.addAttribute("totalGuests", totalGuests);

        return "admin/guest/guestShow";
    }

    @GetMapping("/admin/guest/delete/{id}")
    public String deleteGuest(@PathVariable Long id, RedirectAttributes redirectAttributes) throws NotFoundException {
        guestServices.delete(id);
        redirectAttributes.addFlashAttribute("success", "Guest deleted successfully");
        return "redirect:/admin/guestlist";
    }




    @GetMapping("/admin/roomlist")
    public String roomList(Model model) {

        List<Room> rooms = roomServices.getAllRooms();
        Long totalRooms = roomServices.countTotalRooms();

        model.addAttribute("rooms", rooms);
        model.addAttribute("totalRooms", totalRooms);

        return "admin/room/roomList";
    }

    @GetMapping("/admin/addRoom")
    public String addRoom(Model model) {
        model.addAttribute("Room", new Room());
        model.addAttribute("pageTitle", "Add new room");

        model.addAttribute("Types", typeServices.getAllTypes());
        model.addAttribute("Hotels", hotelServices.getAllHotels());
        return "admin/room/roomAdd";
    }

    @PostMapping("/admin/room/save")
    public String savePost(@ModelAttribute(name = "room") Room room, RedirectAttributes ra) {

        Room savedRoom = roomServices.saveRoom(room);

        ra.addFlashAttribute("message","The room has been saved successfully.");

        return "redirect:/admin/roomlist";
    }

    @GetMapping("/admin/room/edit/{id}")
    public String editRoom(Model model, @PathVariable("id") Long id) throws NotFoundException {

        Room roomToEdit = roomServices.getRoomById(id);
        model.addAttribute("Room", roomToEdit);
        model.addAttribute("pageTitle", "Edit room");

        model.addAttribute("Types", typeServices.getAllTypes());
        model.addAttribute("Hotels", hotelServices.getAllHotels());
        return "admin/room/roomAdd";
    }

    @GetMapping("/admin/room/delete/{id}")
    public String deleteRoom(@PathVariable("id") Long id, RedirectAttributes ra) throws NotFoundException {

        roomServices.delete(id);

        ra.addFlashAttribute("message","The room has been deleted successfully.");

        return "redirect:/admin/roomlist";
    }

    @GetMapping("/admin/login")
    public String login() {
        return "admin/login";
    }
    
}
